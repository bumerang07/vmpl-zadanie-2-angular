import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { Category } from "@models/category.model";
import { Router } from '@angular/router';

import { environment } from "@src/environments/environment";

@Injectable({providedIn: 'root'})
export class CategoriesService {

  // variable to store edited category 
  private editCategory: Category = null;

  // keep token that allows to send new category
  private newCategoryToken: string = null;

  // keep token that allows to update category
  private updateCategoryToken: string = null;

  // keep token that allows to delete category
  private deleteCategoryToken: string = null;

  // url to our backend
  private host = environment.apiUrl;

  // we don't want to share this list but only it's copy
  private categories: Category[] = [];

  // this will be new subject that can be subscribed by components
  private categoriesUpdated = new Subject<{categories: Category[], categoriesCount: number}>();


  // this will be new category to edit
  private categoryForEditUpdated = new Subject<Category>();

  // this will be new category to edit
  private categoryToShowUpdated = new Subject<Category>();

  // this will be new category to edit
  private newCategoryTokenRecived = new Subject();


  // bindin HttpClient to private property
  constructor(private http: HttpClient, private router: Router){}
  

  setNewCategories(categoriesCount) {
    // send copy of updated list
    this.categoriesUpdated.next({categories: [...this.categories], categoriesCount: categoriesCount});
  }

  // public method for returning our categories list
  getCategories(newPage: number, perPage:number) {

    newPage ?? 0;
    perPage ?? 5;

    this.http.get<{categories: Category[], message: string, newCategoryToken: any, categoriesCount: number}>(this.host+'category/?page='+newPage+'&per_page='+perPage, {headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}) // i can add more specyfik data wich will came back for server, but I wont't do this now - becouse of testing 
      .subscribe((responseData: any) => {

        this.categories = responseData.categories;

        // get the csrf token for adding new category
        this.newCategoryToken = responseData.newCategoryToken.value;

        this.setNewCategories(responseData.categoriesCount);
      });

  }

  // public method that allows components to subscribe to our subject
  getCategoriesUpdateListener(){
    return this.categoriesUpdated.asObservable();
  }

  // public method that allows components to subscribe to our subject
  getCategoryToShowListener(){
    return this.categoryToShowUpdated.asObservable();
  }

  // public method that allow to add new category to our list, but only if it is type of Category model
  addCategory(newCategory: Category ){
    // create a new formto send data to server
    const formData: FormData = new FormData;
    formData.set("category[name]", newCategory.name);
    formData.set("category[description]", newCategory.description);
    formData.set("_token", this.newCategoryToken);
    formData.set("category[_token]", this.newCategoryToken);

    this.http.post<{categories: Category[], categoriesCount: number}>(this.host+'category/new', formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {
        this.categories = responseData.categories;
        
        this.setNewCategories(responseData.categoriesCount);

        // after update redirect user to categories list
        this.router.navigate(['categories']);
    } );
  }
  
  
  getCategoryForEdit(id: string){
    return this.http.get<{category: Category, _token: string, _token_delete: string}>(
      this.host+'category/'+id+'/edit', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        // console.log(responseData);
        this.updateCategoryToken = responseData._token;
        this.deleteCategoryToken = responseData._token_delete;
        this.categoryForEditUpdated.next( responseData.category );
        // return  responseData;
      });
  }
  
  getCategoryToShow(id: string){
    return this.http.get<{category: Category}>(
      this.host+'category/'+id, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        console.log(responseData);
        // this.updateCategoryToken = responseData._token;
        // this.deleteCategoryToken = responseData._token_delete;
        this.categoryToShowUpdated.next( responseData.category );
      });
  }
  
  getNewCategory(){
    return this.http.get<{response: any}>(
      this.host+'category/new', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        this.newCategoryToken = responseData._token;
        this.newCategoryTokenRecived.next();
      });
  }
  
  // public method that allows components to subscribe to our subject
  getNewCategorysListener(){
    return this.newCategoryTokenRecived.asObservable();
  }
    
  
  // public method that allows components to subscribe to our subject
  getCategorysForEditListener(){
    return this.categoryForEditUpdated.asObservable();
  }
    

  // public method that allow to add new category to our list, but only if it is type of Category model
  updateCategory(updateCategory: Category ){
    // create a new formto send data to server
    const formData: FormData = new FormData;
    formData.set("category[name]", updateCategory.name);
    formData.set("category[description]", updateCategory.description);
    formData.set("_token", this.updateCategoryToken);
    formData.set("category[_token]", this.updateCategoryToken);

    this.http.post<{categories: Category[],message: any, token: string, categoriesCount: number}>(this.host+'category/'+updateCategory.id+'/edit', formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {
        this.categories = responseData.categories;
        this.setNewCategories(responseData.categoriesCount);

        // after update redirect user to categories list
        this.router.navigate(['categories']);
    } );
  }

  deleteCategory(categoryID: string){
    // first get new delete category token
    this.http.get<{category: Category,message: any, token: string, _token_delete: string}>
      (this.host+'category/'+categoryID+'/edit', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {

        this.editCategory = responseData.category;
        this.deleteCategoryToken = responseData._token_delete;

        //create fake form to send delete request
        const formData: FormData = new FormData;
        formData.set("_method", 'DELETE');
        formData.set("_token", this.deleteCategoryToken);

        // send delete request form
        this.http.post<{categories: Category[], categoriesCount: number}>(this.host+'category/'+this.editCategory.id, formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
        .subscribe( (responseData: any) => {


          console.log('After delete');
          console.log(responseData);
          // get new categories list, and redirect to it page
          this.categories = responseData.categories;
          this.setNewCategories(responseData.categoriesCount);

          // after update redirect user to categories list
          this.router.navigate(['categories']);
        } );
    } );
  }

}