import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { Book } from "@models/book.model";
import { Router } from '@angular/router';

import { environment } from "@src/environments/environment";


@Injectable({providedIn: 'root'})
export class BooksService {

  // variable to store edited book 
  private editBook: Book = null;

  // keep token that allows to send new book
  private newBookToken: string = null;

  // keep token that allows to update book
  private updateBookToken: string = null;

  // keep token that allows to delete book
  private deleteBookToken: string = null;

  // url to our backend
  private host = environment.apiUrl;

  // we don't want to share this list but only it's copy
  private books: Book[] = [];

  // this will be new subject that can be subscribed by components
  private booksUpdated = new Subject<{books: Book[], booksCount: number}>();


  // this will be new book to edit
  private bookForEditUpdated = new Subject<{book:Book, categories: any}>();

  // this will be new book to edit
  private bookToShowUpdated = new Subject<Book>();

  // this will be new book to edit
  private newBookTokenRecived = new Subject();


  // bindin HttpClient to private property
  constructor(private http: HttpClient, private router: Router){}
  

  setNewBooks(booksCount) {
    // send copy of updated list
    this.booksUpdated.next({books: [...this.books], booksCount: booksCount});
  }

  // public method for returning our books list
  getBooks(newPage: string, perPage:string) {

    newPage ?? '0';
    perPage ?? '5';

    this.http.get<{books: Book[], message: string, newBookToken: any, booksCount: number}>(this.host+'book/?page='+newPage+'&per_page='+perPage, {headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}) // i can add more specyfik data wich will came back for server, but I wont't do this now - becouse of testing 
      .subscribe((responseData: any) => {

        this.books = responseData.books;

        // get the csrf token for adding new book
        this.newBookToken = responseData.newBookToken.value;

        this.setNewBooks(responseData.booksCount);
      });

  }

  // public method that allows components to subscribe to our subject
  getBooksUpdateListener(){
    return this.booksUpdated.asObservable();
  }

  // public method that allows components to subscribe to our subject
  getBookToShowListener(){
    return this.bookToShowUpdated.asObservable();
  }

  // public method that allow to add new book to our list, but only if it is type of Book model
  addBook(newBook: Book ){
    // create a new formto send data to server
    const formData: FormData = new FormData;
    formData.set("book[title]", newBook.title);
    formData.set("book[description]", newBook.description);
    formData.set("book[author]", newBook.author);
    formData.set("book[price]", newBook.price);
    formData.set("book[imageFile][file]", newBook.imageFile);
    for(let i=0; i < newBook.categories.length; i++){
      formData.set("book[categories]["+i+"]", newBook.categories[i]);
    }
    console.log(formData);
    formData.set("_token", this.newBookToken);
    formData.set("book[_token]", this.newBookToken);

    this.http.post<{books: Book[], booksCount: number}>(this.host+'book/new', formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {
        this.books = responseData.books;
        
        this.setNewBooks(responseData.booksCount);

        // after update redirect user to books list
        this.router.navigate(['']);
    } );
  }
  
  
  getBookForEdit(id: string){
    return this.http.get<{book: Book, _token: string, _token_delete: string}>(
      this.host+'book/'+id+'/edit', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        // console.log(responseData);
        this.updateBookToken = responseData._token;
        this.deleteBookToken = responseData._token_delete;
        this.bookForEditUpdated.next( {book: responseData.book, categories: responseData.allCategories} );
        // return  responseData;
      });
  }
  
  getBookToShow(id: string){
    return this.http.get<{book: Book}>(
      this.host+'book/'+id, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        console.log(responseData);
        // this.updateBookToken = responseData._token;
        // this.deleteBookToken = responseData._token_delete;
        this.bookToShowUpdated.next( responseData.book );
      });
  }
  
  getNewBook(){
    return this.http.get<{response: any}>(
      this.host+'book/new', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true}
      ).subscribe((responseData: any) => {
        this.newBookToken = responseData._token;
        this.newBookTokenRecived.next( responseData.allCategories );
      });
  }
  
  // public method that allows components to subscribe to our subject
  getNewBooksListener(){
    return this.newBookTokenRecived.asObservable();
  }
    
  
  // public method that allows components to subscribe to our subject
  getBooksForEditListener(){
    return this.bookForEditUpdated.asObservable();
  }
    

  // public method that allow to add new book to our list, but only if it is type of Book model
  updateBook(updateBook: Book ){
    // create a new formto send data to server
    const formData: FormData = new FormData;
    formData.set("book[title]", updateBook.title);
    formData.set("book[description]", updateBook.description);
    formData.set("book[author]", updateBook.author);
    formData.set("book[price]", updateBook.price);
    formData.set("book[imageFile][file]", updateBook.imageFile);
    for(let i=0; i < updateBook.categories.length; i++){
      formData.set("book[categories]["+i+"]", updateBook.categories[i]);
    }
    formData.set("_token", this.updateBookToken);
    formData.set("book[_token]", this.updateBookToken);


    this.http.post<{books: Book[],message: any, token: string, booksCount: number}>(this.host+'book/'+updateBook.id+'/edit', formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {
        this.books = responseData.books;
        this.setNewBooks(responseData.booksCount);

        // after update redirect user to books list
        this.router.navigate(['']);
    } );
  }

  deleteBook(bookID: string, booksPage: string = '0', booksPerPage: string = '0'){
    // first get new delete book token
    this.http.get<{book: Book,message: any, token: string, _token_delete: string}>
      (this.host+'book/'+bookID+'/edit', { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
      .subscribe( (responseData) => {

        this.editBook = responseData.book;
        this.deleteBookToken = responseData._token_delete;

        //create fake form to send delete request
        const formData: FormData = new FormData;
        formData.set("_method", 'DELETE');
        formData.set("_token", this.deleteBookToken);

        // send delete request form
        this.http.post<{books: Book[], booksCount: number}>(this.host+'book/'+this.editBook.id, formData, { headers: {'X-Requested-With': "XMLHttpRequest"}, withCredentials: true})
        .subscribe( (responseData: any) => {

          // get new books list, and redirect to it page
          // this.books = responseData.books;
          // this.setNewBooks(responseData.booksCount);

          // after update redirect user to books list
          this.router.navigate(['page', booksPage, 'per_page', booksPerPage]);
        } );
    } );
  }

}