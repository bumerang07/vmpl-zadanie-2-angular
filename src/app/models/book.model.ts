export interface Book {
  id: string;
  title: string;
  author: string;
  // price: number;
  price: string;
  description: string;
  categories: any;
  imageFile: any;
}