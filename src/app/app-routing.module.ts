import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { CategoryCreateComponent } from '@components/categories/category-create/category-create.component';
import { CategoryListComponent } from '@components/categories/category-list/category-list.component';
import { CategoryEditComponent } from '@components/categories/category-edit/category-edit.component';
import { CategoryShowComponent } from '@components/categories/category-show/category-show.component';
import { BookListComponent } from './components/books/book-list/book-list.component';
import { BookCreateComponent } from './components/books/book-create/book-create.component';
import { BookEditComponent } from './components/books/book-edit/book-edit.component';
import { BookShowComponent } from './components/books/book-show/book-show.component';

const routes: Routes = [
  { path: 'categories', component: CategoryListComponent},
  { path: 'categories/create', component: CategoryCreateComponent},
  { path: 'categories/edit/:categoryId', component: CategoryEditComponent},
  { path: 'categories/show/:categoryId', component: CategoryShowComponent},

  { path: '', component: BookListComponent},
  { path: 'page/:pageNumber/per_page/:perPage', component: BookListComponent},
  { path: 'books/create', component: BookCreateComponent},
  { path: 'books/edit/:bookId', component: BookEditComponent},
  { path: 'books/show/:bookId', component: BookShowComponent},
  // { path: 'categories/show/:categoryId', component: CategoryCreateComponent}, // need to create categoryShowComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{

}