import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Category } from "@models/category.model";
import { CategoriesService } from "@services/categories.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html'
})
export class CategoryEditComponent implements OnInit, OnDestroy {

  // an empty Category object so we can use it in fomr
  category: Category = {id: null, name: null, description: null, books: []};

  // variable that allows subscription to categories changes
  private categoryEditSubscription: Subscription;


  private categoryId: string = null;

  // form: FormGroup;
  
  constructor (public categoriesService: CategoriesService, public route: ActivatedRoute){}


  ngOnInit() {
    this.route.paramMap.subscribe( (paramsMap) => {
      if(paramsMap.has('categoryId')) {
        this.categoryId = paramsMap.get('categoryId');

        this.categoryEditSubscription =  this.categoriesService.getCategorysForEditListener().subscribe(
            
            // this is category send by CategoriesService after it will get it from a server
            (category: Category)  => {

            // asign newly send category as inner value
            this.category = category;
          }
        );

        this.categoriesService.getCategoryForEdit(this.categoryId);

      }

    });
  }


  // When the form is submited
  onUpdateCategory(form: NgForm) {
    // if this form isn't valid just do nothing
    if(form.invalid){
      return;
    }

    // full fill this category with form data
    // with only values that i want to update here
    this.category.name = form.value.name;
    this.category.description = form.value.description;

    // send newly updated category to the CategoriesService via it's public method
    this.categoriesService.updateCategory(this.category);
    form.resetForm();
  }

  onDeleteCategory(){
    this.categoriesService.deleteCategory(this.category.id);
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.categoryEditSubscription.unsubscribe();
  }
}