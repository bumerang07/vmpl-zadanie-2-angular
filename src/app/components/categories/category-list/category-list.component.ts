import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { Category } from "@models/category.model";
import { CategoriesService } from "@services/categories.service";
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-category-list',
  templateUrl: 'category-list.component.html',
  styleUrls: ['category-list.component.css']
})
export class CategoryListComponent implements OnInit, OnDestroy {

  categoriesPerPAge = 5;

  totalCategories: number = 0;

  // variable that allows subscription to categories changes
  private categoriesSubscription: Subscription;

  // local list of categories - variables of type Category
  categories: Category[] = [];

  constructor (public categoriesService: CategoriesService) {}

  ngOnInit(){
    // let's inicialize local categories list with categories stored in it's service
    // this.categories = this.categoriesService.getCategories();
    
    // let's subscribe for categoriesService categories list change
    // I use a sepparated variable so I can unsubscribe when this component is destroyed
    this.categoriesSubscription = this.categoriesService.getCategoriesUpdateListener().subscribe(
      // those are categories send by CategoriesService on categories list update
      // I make sure it's list of Category model objects
      (response: any)  => {

        // asign newly send categories to this component categories list
        this.categories = response.categories;

        //get te total number of articless
        this.totalCategories = response.categoriesCount;
      }
    );

    // Make sure that CategoriesService will check for new categories
    this.categoriesService.getCategories(0, 5);
  }

  setPageSizeOptions(pageData: PageEvent){
    console.log('pageSwitched');
    console.log(pageData);
    console.log('pageSwitched');
    // this.categoriesService.getCategories();

    this.categories = [];
    this.categoriesService.getCategories(pageData.pageIndex, pageData.pageSize);
  }

  onDeleteCategory(categoryId){
    this.categoriesService.deleteCategory(categoryId);
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.categoriesSubscription.unsubscribe();
  }


}