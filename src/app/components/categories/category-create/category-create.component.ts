import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Category } from "@models/category.model";
import { CategoriesService } from "@services/categories.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html'
})
export class CategoryCreateComponent implements OnInit, OnDestroy {

  // an empty Category object so we can use it in fomr
  category: Category = {id: null, name: null, description: null, books: []};

  // variable that allows subscription to categories changes
  private categoriesSubscription: Subscription;

  
  constructor (public categoriesService: CategoriesService, public route: ActivatedRoute){}


  ngOnInit() {
    this.categoriesSubscription =  this.categoriesService.getNewCategorysListener().subscribe();
    this.categoriesService.getNewCategory();
  }


  // When the form is submited
  onAddCategory(form: NgForm) {
    // if this form isn't valid just do nothing
    if(form.invalid){
      return;
    }

    // full fill this category with form data - can be helpful later on 
    this.category = {
      id: null,
      name: form.value.name,
      description: form.value.description,
      books: []
    };

    // send newly added category to the CategoriesService via it's public method
    this.categoriesService.addCategory(this.category);
    form.resetForm();
  }

  ngOnDestroy(){
    this.categoriesSubscription.unsubscribe();
  }
}