import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Category } from "@models/category.model";
import { CategoriesService } from "@services/categories.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category-show',
  templateUrl: './category-show.component.html'
})
export class CategoryShowComponent implements OnInit, OnDestroy {

  // an empty Category object so we can store the downloaded one
  category: Category = {id: null, name: null, description: null, books: []};

  // variable that allows subscription to category download
  private categoryShowSubscription: Subscription;


  private categoryId: string = null;

  constructor (public categoriesService: CategoriesService, public route: ActivatedRoute){}


  ngOnInit() {
    this.route.paramMap.subscribe( (paramsMap) => {
      if(paramsMap.has('categoryId')) {

        this.categoryId = paramsMap.get('categoryId');

        this.categoryShowSubscription =  this.categoriesService.getCategoryToShowListener().subscribe(

            // this is category send by CategoriesService after it will get it from a server
            (category: Category)  => {

            console.log('Category show controller');
            console.log(category);
            
            // asign newly send category as inner value
            this.category = category;
          }
        );

        this.categoriesService.getCategoryToShow(this.categoryId);

      }

    });
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.categoryShowSubscription.unsubscribe();
  }
}