import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { Book } from "@models/book.model";
import { BooksService } from "@services/books.service";
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-book-list',
  templateUrl: 'book-list.component.html',
  styleUrls: ['book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {

  booksPerPage: string = '5';
  currentPage:string = '0';

  totalBooks: number = 0;

  // variable that allows subscription to books changes
  private booksSubscription: Subscription;

  // local list of books - variables of type Book
  books: Book[] = [];

  constructor (public booksService: BooksService, public route: ActivatedRoute, private router: Router) {}

  ngOnInit(){
    
    // check our route if there is page number && per page then load only those books, that will cover that range
    this.route.paramMap.subscribe( (paramsMap) => {
      if(paramsMap.has('pageNumber') && paramsMap.has('perPage')) {
        this.currentPage = paramsMap.get('pageNumber');
        this.booksPerPage = paramsMap.get('perPage');
        let currentPageNumber = parseInt(this.currentPage);
        if(currentPageNumber > 0) this.currentPage = (currentPageNumber - 1).toString();
        this.booksService.getBooks(this.currentPage, this.booksPerPage);
      } else {
        this.booksService.getBooks('0', '5');
      }
    });

    
    // let's subscribe for booksService books list change
    // I use a sepparated variable so I can unsubscribe when this component is destroyed
    this.booksSubscription = this.booksService.getBooksUpdateListener().subscribe(
      // those are books send by BooksService on books list update
      // I make sure it's list of Book model objects
      (response: any)  => {

        // asign newly send books to this component books list
        this.books = response.books;

        //get te total number of articless
        this.totalBooks = response.booksCount;
      }
    );

    // Make sure that BooksService will check for new books
    
  }

  setPageSizeOptions(pageData: PageEvent){
   
    this.currentPage = (pageData.pageIndex+1).toString();
    this.booksPerPage = (pageData.pageSize).toString();
    
    this.router.navigate(['page', this.currentPage, 'per_page', this.booksPerPage]);
    // this.router.navigate(['books']);
  }

  onDeleteBook(bookId: string){
    this.booksService.deleteBook(bookId, this.currentPage, this.booksPerPage);
    ;
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.booksSubscription.unsubscribe();
  }


}