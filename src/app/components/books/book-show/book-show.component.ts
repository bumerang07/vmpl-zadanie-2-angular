import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Book } from "@models/book.model";
import { BooksService } from "@services/books.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-book-show',
  templateUrl: './book-show.component.html'
})
export class BookShowComponent implements OnInit, OnDestroy {

  // an empty Book object so we can store the downloaded one
  book: Book = { id: null, title: null, description: null, author: null, price: null, categories: [], imageFile: null };

  // variable that allows subscription to book download
  private bookShowSubscription: Subscription;


  private bookId: string = null;

  constructor (public booksService: BooksService, public route: ActivatedRoute){}


  ngOnInit() {
    this.route.paramMap.subscribe( (paramsMap) => {
      if(paramsMap.has('bookId')) {

        this.bookId = paramsMap.get('bookId');

        this.bookShowSubscription =  this.booksService.getBookToShowListener().subscribe(

            // this is book send by BooksService after it will get it from a server
            (book: Book)  => {

            console.log('Book show controller');
            console.log(book);
            
            // asign newly send book as inner value
            this.book = book;
          }
        );

        this.booksService.getBookToShow(this.bookId);

      }

    });
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.bookShowSubscription.unsubscribe();
  }
}