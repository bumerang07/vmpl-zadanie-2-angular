import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Book } from "@models/book.model";
import { BooksService } from "@services/books.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { mimeType } from '@src/app/validators/mime-type.validator';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html'
})
export class BookEditComponent implements OnInit, OnDestroy {

  // an empty Book object so we can use it in fomr
  book: Book = { id: null, title: null, description: null, author: null, price: null, categories: [], imageFile: null };

  // all categories
  categories: any = [];

  // only this book categories
  bookCategories: any = [];

  // url of cover image
  bookCover: string;

  form: FormGroup;

  // variable that allows subscription to books changes
  private bookEditSubscription: Subscription;


  private bookId: string = null;

  // form: FormGroup;
  
  constructor (public booksService: BooksService, public route: ActivatedRoute){}


  ngOnInit() {
    this.form = new FormGroup({
      'title' : new FormControl(null,{
        validators: [Validators.required],
      }),
      'description': new FormControl(null, {
        validators: [Validators.required]
      }),
      'author': new FormControl(null, {
        validators: [Validators.required]
      }),
      'price': new FormControl(null, {
        validators: [Validators.required]
      }),
      'categories': new FormControl([]),
      'image': new FormControl(null, {
        validators: [Validators.required],

        // using custom mimetype validator - allows only png and jp(e)g images
        asyncValidators: [mimeType]
      }),
    })

    this.route.paramMap.subscribe( (paramsMap) => {
      if(paramsMap.has('bookId')) {
        this.bookId = paramsMap.get('bookId');

        this.bookEditSubscription =  this.booksService.getBooksForEditListener().subscribe(
            
            // this is response will have  
            // book send by BooksService after it will get it from a server
            // and all categories
            (response: any)  => {
            
              this.categories = response.categories;

              // asign newly send book as inner value
              this.book = response.book;

              
              
              this.form.setValue({
                'title': this.book.title,
                'author': this.book.author,
                'price': this.book.price,
                'description': this.book.description,
                'categories': this.categories,
                'image': this.book.imageFile,
              });

              //set book categories as default values
              for (let i=0; i< this.book.categories.length; i++){
                this.bookCategories[i] = this.book.categories[i].id;
              }
              this.form.get('categories').setValue(this.bookCategories);
            }
          
        );

        this.booksService.getBookForEdit(this.bookId);

      }

    });
  }

  onImagePicked(event: Event){

    //get uploaded image
    const file = (event.target as HTMLInputElement).files[0];
    // storre it in Reactive Form field
    this.form.patchValue({image: file});

    // valideate that field
    this.form.get('image').updateValueAndValidity();

    //create a new reader to get file path to enable image preview
    const reader = new FileReader();

    // when file will be loaded return it's url to bookCover so i could display it
    reader.onload = () =>{
      this.bookCover = reader.result as string;
    }
    // read that image
    reader.readAsDataURL(file);
  }



  // When the form is submited
  onUpdateBook() {
    // if this form isn't valid just do nothing
    if(this.form.invalid){
      return;
    }

    // full fill this book with form data
    // with only values that i want to update here
    this.book.title = this.form.value.title;
    this.book.description = this.form.value.description;
    this.book.author = this.form.value.author;
    this.book.price = this.form.value.price;
    this.book.categories = this.form.value.categories;
    this.book.imageFile = this.form.value.image;
    
    // send newly updated book to the BooksService via it's public method
    this.booksService.updateBook(this.book);
    this.form.reset();
  }

  onDeleteBook(){
    this.booksService.deleteBook(this.book.id);
  }

  ngOnDestroy() {
    // I don't need to watch it any more
    this.bookEditSubscription.unsubscribe();
  }
}