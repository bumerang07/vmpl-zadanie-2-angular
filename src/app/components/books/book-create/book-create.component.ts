import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { FormGroup } from '@angular/forms';

import { Book } from "@models/book.model";
import { BooksService } from "@services/books.service";

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { mimeType } from "@src/app/validators/mime-type.validator";

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html'
})
export class BookCreateComponent implements OnInit, OnDestroy {

  // an empty Book object so we can use it in fomr
  book: Book = { id: null, title: null, description: null, author: null, price: null, categories: [], imageFile: null };

  categories: any = [];

  // url of cover image
  bookCover: string;

  form: FormGroup;

  // variable that allows subscription to books changes
  private booksSubscription: Subscription;

  
  constructor (public booksService: BooksService, public route: ActivatedRoute){}


  ngOnInit() {
    this.form = new FormGroup({
      'title' : new FormControl(null,{
        validators: [Validators.required],
      }),
      'description': new FormControl(null, {
        validators: [Validators.required]
      }),
      'author': new FormControl(null, {
        validators: [Validators.required]
      }),
      'price': new FormControl(null, {
        validators: [Validators.required]
      }),
      'categories': new FormControl([]),
      'image': new FormControl(null, {
        validators: [Validators.required],

        // using custom mimetype validator - allows only png and jp(e)g images
        asyncValidators: [mimeType]
      }),
    })
    this.booksSubscription =  this.booksService.getNewBooksListener().subscribe(
      (catgories) => {
        this.categories = catgories;
        this.form.setValue({
          'title': null,
          'author': null,
          'price': null,
          'description': null,
          'categories': this.categories,
          'image': null
        })
      }
    );
    this.booksService.getNewBook();
  }

  onImagePicked(event: Event){

    //get uploaded image
    const file = (event.target as HTMLInputElement).files[0];
    // storre it in Reactive Form field
    this.form.patchValue({image: file});

    // valideate that field
    this.form.get('image').updateValueAndValidity();

    //create a new reader to get file path to enable image preview
    const reader = new FileReader();

    // when file will be loaded return it's url to bookCover so i could display it
    reader.onload = () =>{
      this.bookCover = reader.result as string;
    }
    // read that image
    reader.readAsDataURL(file);
  }

  // When the form is submited
  onAddBook() {
    // if this form isn't valid just do nothing
    if(this.form.invalid){
      return;
    }

    // full fill this book with form data - can be helpful later on 
    this.book = {
      id: null,
      title: this.form.value.title,
      description: this.form.value.description,
      author: this.form.value.author,
      price: this.form.value.price,
      categories: this.form.value.categories,
      imageFile: this.form.value.image,
    };

    // send newly added book to the BooksService via it's public method
    this.booksService.addBook(this.book);
    this.form.reset();
  }

  ngOnDestroy(){
    this.booksSubscription.unsubscribe();
  }
}